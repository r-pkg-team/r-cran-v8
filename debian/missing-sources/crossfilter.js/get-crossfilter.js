#!/bin/sh
#https://crossfilter.github.io/crossfilter/

wget -q -N https://raw.githubusercontent.com/square/crossfilter/master/crossfilter.js
SRC=crossfilter.js
COMPRESSED=`echo $SRC | sed 's/\.js$/.min&/'`
closure-compiler  --charset 'utf-8' --js $SRC --js_output_file $COMPRESSED
# ?? yui-compressor $SRC > $COMPRESSED
# ?? uglifyjs $SRC --mangle > $COMPRESSED
wget -q -N https://raw.githubusercontent.com/square/crossfilter/master/LICENSE
